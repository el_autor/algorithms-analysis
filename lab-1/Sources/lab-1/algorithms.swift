public var maxRecursionDepth: Int!

func removeLastSymb(_ str: String) -> String {
    return String(str[str.startIndex..<str.index(before: str.endIndex)])
}

func recursiveLevenstein(_ strA: String, _ strB: String) -> Int {
    if strA.count == 0 || strB.count == 0 {
        return abs(strA.count - strB.count)
    }

    let lastSymbsAreEqual = (strA.last == strB.last) ? 0 : 1

    return min(recursiveLevenstein(strA, removeLastSymb(strB)) + 1,
               recursiveLevenstein(removeLastSymb(strA), strB) + 1,
               recursiveLevenstein(removeLastSymb(strA), removeLastSymb(strB)) + lastSymbsAreEqual)
}



func recursiveLevensteinDepth(_ strA: String, _ strB: String, _ depth: Int ) -> Int {
    if depth > maxRecursionDepth {
        maxRecursionDepth = depth
    }

    if strA.count == 0 || strB.count == 0 {
        return abs(strA.count - strB.count)
    }

    let lastSymbsAreEqual = (strA.last == strB.last) ? 0 : 1

    return min(recursiveLevensteinDepth(strA, removeLastSymb(strB), depth + 1) + 1, 
               recursiveLevensteinDepth(removeLastSymb(strA), strB, depth + 1) + 1,
               recursiveLevensteinDepth(removeLastSymb(strA), removeLastSymb(strB), depth + 1) + lastSymbsAreEqual)
}

func recursiveOptimizedLevenstein(_ strA: String, _ strB: String, _ matrix: inout Array<Array<Int?>>) -> Int {
    guard matrix[strA.count][strB.count] == nil else { return matrix[strA.count][strB.count]! }
    
    if strA.count == 0 || strB.count == 0 {
        matrix[strA.count][strB.count] = abs(strA.count - strB.count)
        return matrix[strA.count][strB.count]!
    }

    let lastSymbsAreEqual = (strA.last == strB.last) ? 0 : 1
   
    matrix[strA.count][strB.count] = min(recursiveOptimizedLevenstein(strA, removeLastSymb(strB), &matrix) + 1,
                                         recursiveOptimizedLevenstein(removeLastSymb(strA), strB, &matrix) + 1,
                                         recursiveOptimizedLevenstein(removeLastSymb(strA), removeLastSymb(strB), &matrix) + lastSymbsAreEqual)

    return matrix[strA.count][strB.count]!
}

func recursiveOptimizedLevensteinDepth(_ strA: String, _ strB: String, _ matrix: inout Array<Array<Int?>>, _ depth: Int) -> Int {
    if depth > maxRecursionDepth {
        maxRecursionDepth = depth
    }

    guard matrix[strA.count][strB.count] == nil else { return matrix[strA.count][strB.count]! }
    
    if strA.count == 0 || strB.count == 0 {
        matrix[strA.count][strB.count] = abs(strA.count - strB.count)
        return matrix[strA.count][strB.count]!
    }

    let lastSymbsAreEqual = (strA.last == strB.last) ? 0 : 1
   
    matrix[strA.count][strB.count] = min(recursiveOptimizedLevensteinDepth(strA, removeLastSymb(strB), &matrix, depth + 1) + 1,
                                         recursiveOptimizedLevensteinDepth(removeLastSymb(strA), strB, &matrix, depth + 1) + 1,
                                         recursiveOptimizedLevensteinDepth(removeLastSymb(strA), removeLastSymb(strB), &matrix, depth + 1) + lastSymbsAreEqual)

    return matrix[strA.count][strB.count]!
}

func matrixLevenstein(_ strA: String, _ strB: String, _ matrix: inout Array<Array<Int?>>) -> Int {
    for i in 0...strA.count {
        for j in 0...strB.count {
            if i == 0 {
                matrix[i][j] = j
            }
            else if j == 0 && i > 0 {
                matrix[i][j] = i 
            }
            else {
                let flag = (strA[strA.index(strA.startIndex, offsetBy: i - 1)] == strB[strB.index(strB.startIndex, offsetBy: j - 1)]) ? 0 : 1
                matrix[i][j] = min(matrix[i][j - 1]! + 1, 
                                   matrix[i - 1][j]! + 1,
                                   matrix[i - 1][j - 1]! + flag)
            }
        }
    }

    return matrix[strA.count][strB.count]!
}

func isTransposition(_ strA: String, _ strB: String, _ i: Int, _ j: Int) -> Bool {
    return (strA[strA.index(strA.startIndex, offsetBy: i - 1)] == strB[strB.index(strB.startIndex, offsetBy: j - 2)] &&
            strA[strA.index(strA.startIndex, offsetBy: i - 2)] == strB[strB.index(strB.startIndex, offsetBy: j - 1)])
} 

func matrixLamerauLevenstein(_ strA: String, _ strB: String, _ matrix: inout Array<Array<Int?>>) -> Int {
    for i in 0...strA.count {
        for j in 0...strB.count {
            if i == 0 || j == 0 {
                matrix[i][j] = max(i, j)
            }
            else {
                let flagEqual = (strA[strA.index(strA.startIndex, offsetBy: i - 1)] == strB[strB.index(strB.startIndex, offsetBy: j - 1)]) ? 0 : 1
             
                if j > 1 && i > 1 && isTransposition(strA, strB, i, j) {
                    matrix[i][j] = min(matrix[i][j - 1]! + 1, 
                                       matrix[i - 1][j]! + 1,
                                       matrix[i - 1][j - 1]! + flagEqual,
                                       matrix[i - 2][j - 2]! + 1)
                }
                else {    
                    matrix[i][j] = min(matrix[i][j - 1]! + 1, 
                                       matrix[i - 1][j]! + 1,
                                       matrix[i - 1][j - 1]! + flagEqual)
                }
            }
        }
    }

    return matrix[strA.count][strB.count]!    
}