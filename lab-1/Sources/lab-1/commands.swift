enum Commands: Int {
    case inputStringA
    case inputStringB
    case recursiveLevenstein
    case optimizedRecursiveLevenstein
    case matrixLevenstein
    case matrixLamerauLevenstein
    case showMenu
    case timeAnalyze
    case testing
    case exit
}