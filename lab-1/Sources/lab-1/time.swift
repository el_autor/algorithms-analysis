import Darwin

func analyzeLength(_ s1: String, _ s2: String, _ length: Int, _ timesToRepeat: UInt64) {
    var start: UInt64!
    var end: UInt64!

    print("\nДлина = \(length)")

    start = mach_absolute_time()
    for _ in 0..<timesToRepeat {
        let _ = recursiveLevenstein(s1, s2)             
    }
    end = mach_absolute_time()

    print("Левенштейн рекурсивно ~", (end - start) / timesToRepeat)


    let matrix = createMatrix(s1.count, s2.count)
    start = mach_absolute_time()
    for _ in 0..<timesToRepeat {
        var copy = matrix
        let _ = recursiveOptimizedLevenstein(s1, s2, &copy)           
    }
    end = mach_absolute_time()

    print("Левенштейн рекурсивно c матрицей ~", (end - start) / timesToRepeat)


    start = mach_absolute_time()
    for _ in 0..<timesToRepeat {
        var copy = matrix
        let _ = matrixLevenstein(s1, s2, &copy)           
    }
    end = mach_absolute_time()

    print("Левенштейн матрично ~", (end - start) / timesToRepeat)


    start = mach_absolute_time()
    for _ in 0..<timesToRepeat {
        var copy = matrix
        let _ = matrixLamerauLevenstein(s1, s2, &copy)           
    }
    end = mach_absolute_time()

    print("Ламерау-Левенштейн матрично ~", (end - start) / timesToRepeat)
}

func analyzeTime() {
    analyzeLength("a","b", 1, 10000)
    analyzeLength("ac","bd", 2, 1000)
    analyzeLength("abc","def", 3, 1000)
    analyzeLength("abcd","efgh", 4, 100)
    analyzeLength("abcde","fghij", 5, 50)
    analyzeLength("abcdef","ghijkl", 6, 25)
    analyzeLength("abcdefg","hijklmn", 7, 10)
    analyzeLength("abcdefgh","ijklmnop", 8, 5)
}