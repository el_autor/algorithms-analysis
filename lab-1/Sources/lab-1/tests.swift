func test(_ s1: String, _ s2: String, _ answerLevenstein: Int, _ answerLamerauLevenstein: Int, _ testNumber: Int) {
    var value: Int = 0

    value = recursiveLevenstein(s1, s2) 
    assert(value == answerLevenstein, "Тест \(testNumber) не пройден! Рекурсивный Левенштейн (\(value) != \(answerLevenstein))")

    let matrix = createMatrix(s1.count, s2.count)
    var copy = matrix
    value = recursiveOptimizedLevenstein(s1, s2, &copy)
    assert(value == answerLevenstein, "Тест \(testNumber) не пройден! Рекурсивно-матричный Левенштейн (\(value) != \(answerLevenstein))")

    copy = matrix
    value = matrixLevenstein(s1, s2, &copy)
    assert(value == answerLevenstein, "Тест \(testNumber) не пройден! Матричный Левенштейн (\(value) != \(answerLevenstein))")

    copy = matrix
    value = matrixLamerauLevenstein(s1, s2, &copy)
    assert(value == answerLamerauLevenstein, "Тест \(testNumber) не пройден! Матричный Ламерау-Левенштейн (\(value) != \(answerLamerauLevenstein))")

    print("Тест \(testNumber) пройден успешно")
}   

func runTests() {
    test("", "", 0, 0, 1)
    test("1234", "2143", 3, 2, 2)
    test("amm", "add", 2, 2, 3)
    test("error", "dog", 4, 4, 4)
    test("", "submission", 10, 10, 5)
    test("mochombo", "", 8, 8, 6)
    test("dfufdfd", "fddfdffdd", 5, 4, 7)
}
