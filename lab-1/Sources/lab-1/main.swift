import Darwin

fileprivate var backAdress = 8
fileprivate var returnSize = 8
fileprivate var intSize = 8

func greeting() {
    let text = """
    Программа для расчета расстояния Левенштейна и Дамерау-Левенштейна.
    """
    print(text)
}

func showMenu() {
    let text = """
    Набор команд:
    1 - Ввести строку A
    2 - Ввести строку B
    3 - Левенштеин рекурсивно
    4 - Левенштейн рекурсивно с оптимизацией
    5 - Левенштейн матрично нерекурсивно
    6 - Дамерау-Левенштейн нерекурсивно
    7 - Показать меню
    8 - Анализ временных затрат
    9 - Тестирование
    10 - Выйти
    """
    print(text)
}

func dataIsInputed(_ strA: String?, _ strB: String?) -> Bool {
    guard strA != nil, strB != nil else { 
        print("Не введены 2 строки")
        return false
    }
    
    return true
}

func createMatrix(_ lenA: Int, _ lenB: Int) -> Array<Array<Int?>> {
    var array = Array<Array<Int?>>()

    for _ in 0...lenA {
        array.append(Array<Int?>(repeating: nil, count: lenB + 1))
    }

    return array
}

func showMatrix(_ matrix: Array<Array<Int?>>) {
    for i in 0..<matrix.count {
        for j in 0..<matrix[i].count {
            print(matrix[i][j]!, terminator: " ")
        }

        print()
    }
}

var a: String?
var b: String?

greeting()
showMenu()

mainLoop: while true {
    let input = readLine()!
    let commandNumber = Int(input)
   
    let command = Commands(rawValue: commandNumber! - 1) 

    switch command {
    case .inputStringA:
        a = readLine()
    case .inputStringB:
        b = readLine()
    case .recursiveLevenstein:
        if !dataIsInputed(a, b) {
            break mainLoop
        }

        let start = mach_absolute_time()
        let result = recursiveLevenstein(a!, b!)                 
        let end = mach_absolute_time()

        maxRecursionDepth = 0
        let _ = recursiveLevensteinDepth(a!, b!, 1)
        
        print("Расстояние Левенштейна:", result)
        print("Процессорное время(тики):", end - start)
        let localVarVolume = 8
        print("Память ~", maxRecursionDepth! * (a!.count + b!.count + backAdress + returnSize + localVarVolume), "байт")
    case .optimizedRecursiveLevenstein:
        if !dataIsInputed(a, b) {
            break mainLoop
        }

        var matrix = createMatrix(a!.count, b!.count)
        let start = mach_absolute_time()
        let result = recursiveOptimizedLevenstein(a!, b!, &matrix)                 
        let end = mach_absolute_time()
        
        maxRecursionDepth = 0
        matrix = createMatrix(a!.count, b!.count)
        let _ = recursiveOptimizedLevensteinDepth(a!, b!, &matrix, 1)
        

        print("Матрица:")
        showMatrix(matrix)
        print("Расстояние Левенштейна:", result)
        print("Время в тиках:", end - start)
        let localVarVolume = 8
        print("Память ~", maxRecursionDepth! * (a!.count + b!.count + backAdress + returnSize + localVarVolume) + intSize * (a!.count + 1) * (b!.count + 1), "байт")
    case .matrixLevenstein:
        if !dataIsInputed(a, b) {
            break mainLoop
        }

        var matrix = createMatrix(a!.count, b!.count)
        let start = mach_absolute_time()
        let result = matrixLevenstein(a!, b!, &matrix)
        let end = mach_absolute_time()

        print("Матрица:")
        showMatrix(matrix)
        print("Расстояние Левенштейна:", result)
        print("Время в тиках:", end - start)
        let localVarVolume = 8
        print("Память ~", a!.count + b!.count + localVarVolume + intSize * (a!.count + 1) * (b!.count + 1), "байт")
    case .matrixLamerauLevenstein:
        if !dataIsInputed(a, b) {
            break mainLoop
        }

        var matrix = createMatrix(a!.count, b!.count)
        let start = mach_absolute_time()
        let result = matrixLamerauLevenstein(a!, b!, &matrix)
        let end = mach_absolute_time()

        print("Матрица:")
        showMatrix(matrix)
        print("Расстояние Левенштейна:", result)
        print("Время в тиках:", end - start)
        let localVarVolume = 8
        print("Память ~", a!.count + b!.count + localVarVolume + intSize * (a!.count + 1) * (b!.count + 1), "байт")
    case .showMenu:
        showMenu()
    case .timeAnalyze:
        analyzeTime()
    case .testing:
        runTests()
    case .exit:
        break mainLoop
    default:
        ()
    }
}