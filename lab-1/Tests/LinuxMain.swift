import XCTest

import lab_1Tests

var tests = [XCTestCaseEntry]()
tests += lab_1Tests.allTests()
XCTMain(tests)
