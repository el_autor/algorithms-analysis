func linearSearch(dict: [(String, Int)], key: String) -> Int? {
    for pair in dict {
        if pair.0 == key {
            return pair.1
        }
    }
    
    return nil
}

func binarySearch(dict: [(String, Int)], key: String) -> Int? {
    var low = 0
    var high = dict.count
    
    while low <= high {
        let mid = (low + high) / 2
        
        if mid >= dict.count {
            return nil
        }
        
        if key < dict[mid].0 {
            high = mid - 1
        } else if key > dict[mid].0 {
            low = mid + 1
        } else {
            return dict[mid].1
        }
    }
    
    return nil
}

func segmentsSearch(dict: [(String, Int)], segments: [(Character, Int, Int)], key: String) -> Int? {
    let letter = key.first!
    var left = -1, right = -1
    
    for segm in segments {
        if letter == segm.0 {
            left = segm.1
            right = segm.2
            break
        }
    }
    
    for i in left..<right {
        if dict[i].0 == key {
            return dict[i].1
        }
    }
    
    return nil
}

func segmentsSearchWithFrequency(dict: [(String, Int)], key: String) -> Int? {
    for pair in dict {
        if pair.0 == key {
            return pair.1
        }
    }
    
    return nil
}
