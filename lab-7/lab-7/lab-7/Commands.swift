enum Commands: Int {
    case input
    case inputValue
    case linearSearch
    case binarySearch
    case withSegmants
    case segmentsWithFrequency
    case menu
    case exit
}
