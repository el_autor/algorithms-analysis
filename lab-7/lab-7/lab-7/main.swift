import Darwin
import PostgresClientKit

func greeting() {
    let text = """
    Программа для анализа поиска в словаре
    """
    print(text)
}

func showMenu() {
    let text = """
    Набор команд:
    1 - ввести словарь
    2 - ввести значение для поиска
    3 - линейный поиск
    4 - бинарный поиск
    5 - поиск с сегментами
    6 - сегменты с частотностью
    7 - меню
    8 - выйти
    """
    print(text)
}

func totalEntries(_ symbol: Character, _ data: [(String, Int)]) -> Int{
    var totalEntries = 0
    
    for pair in data {
        if pair.0.first! == symbol {
            totalEntries += 1
        }
    }
    
    return totalEntries
}

func getFrequentlySorted(dict: [(String, Int)]) -> [(String, Int)] {
    var newDict: [(String, Int)] = []
    var frequencyDict = Dictionary<Character, Int>()
    
    for pair in dict {
        if frequencyDict.keys.contains(pair.0.first!) {
            frequencyDict[pair.0.first!]! += 1
        } else {
            frequencyDict[pair.0.first!] = 1
        }
    }
    
    let array = Array(frequencyDict).sorted { $0.value > $1.value }
    
    for i in .zero..<array.count {
        newDict.append(contentsOf: dict.filter { $0.0.first! == array[i].key } )
    }
    
    return newDict
}

func getSegments(dict: [(String, Int)]) -> [(Character, Int, Int)] {
    var segments: [(Character, Int, Int)] = []
    var letter = dict[0].0.first!
    var start = 0
    var end = 0
    
    for i in 1..<dict.count {
        if dict[i].0.first! != letter {
            end = i - 1
            segments.append((letter, start, end))
            start = i
            letter = dict[i].0.first!
        }
    }
    
    return segments
}

func getData() -> [(String, Int)] {
    var data: [(String, Int)] = []
    
    do {
        var configuration = PostgresClientKit.ConnectionConfiguration()
        
        configuration.host = "localhost"
        configuration.port = 5432
        configuration.database = "ufc"
        configuration.user = "postgres"
        configuration.ssl = false
        configuration.credential = .md5Password(password: "zVQ2zYTe")
        
        let connection = try PostgresClientKit.Connection(configuration: configuration)
        
        let text = """
        SELECT name
        FROM fighters;
        """
                    
        let statement = try connection.prepareStatement(text: text)
        let response = try statement.execute()
        
        for row in response {
            let columns = try row.get().columns
            let name = try columns[0].string()
            
            data.append((name, Int.random(in: 0...1000)))
        }
    } catch {
        print(error)
    }
    
    return data
}

var loop = true
var data: [(String, Int)] = []
var key: String?

greeting()
showMenu()

while loop {
    let input = readLine()!
    let commandNumber = Int(input)!
    
    let command = Commands(rawValue: commandNumber - 1)

    switch command {
    case .input:
        data = getData()
    case .inputValue:
        print("Введите ключ(имя бойца):")
        key = readLine()!
    case .linearSearch:
        let start = mach_absolute_time()
        let totalStrikes = linearSearch(dict: data, key: key ?? "")
        let end = mach_absolute_time()

        if let totalStrikes = totalStrikes {
            print("Результат - ", totalStrikes)
        } else {
            print("Ключ не найден")
        }
        
        print("Процессорное время(тики):", end - start)
    case .binarySearch:
        let sortedData = data.sorted { $0.0 < $1.0 }
        
        let start = mach_absolute_time()
        let totalStrikes = binarySearch(dict: sortedData, key: key ?? "")
        let end = mach_absolute_time()

        if let totalStrikes = totalStrikes {
            print("Результат - ", totalStrikes)
        } else {
            print("Ключ не найден")
        }
        
        print("Процессорное время(тики):", end - start)
        
    case .withSegmants:
        let sortedData = data.sorted { $0.0 < $1.0 }
        let segments = getSegments(dict: sortedData)
        
        let start = mach_absolute_time()
        let totalStrikes = segmentsSearch(dict: sortedData, segments: segments, key: key ?? "")
        let end = mach_absolute_time()

        if let totalStrikes = totalStrikes {
            print("Результат - ", totalStrikes)
        } else {
            print("Ключ не найден")
        }
        
        print("Процессорное время(тики):", end - start)
    case .segmentsWithFrequency:
        let frequencySorted = getFrequentlySorted(dict: data)
        
        let start = mach_absolute_time()
        let totalStrikes = segmentsSearchWithFrequency(dict: frequencySorted, key: key ?? "")
        let end = mach_absolute_time()

        if let totalStrikes = totalStrikes {
            print("Результат - ", totalStrikes)
        } else {
            print("Ключ не найден")
        }
        
        print("Процессорное время(тики):", end - start)
    case .menu:
        showMenu()
    case .exit:
        loop = false
    default:
        ()
    }
}
