\babel@toc {russian}{}
\contentsline {chapter}{Введение}{2}%
\contentsline {paragraph}{ }{2}%
\contentsline {chapter}{\numberline {1}Аналитическая часть}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {section}{\numberline {1.1}Постановка задачи}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {section}{\numberline {1.2}Простой поиск}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {section}{\numberline {1.3}Бинарный поиск}{4}%
\contentsline {paragraph}{ }{4}%
\contentsline {section}{\numberline {1.4}Использование сегментов}{4}%
\contentsline {paragraph}{ }{4}%
\contentsline {section}{\numberline {1.5}Использование сегментов, сформированных по принципу частотности}{4}%
\contentsline {paragraph}{ }{4}%
\contentsline {section}{Введение}{5}%
\contentsline {paragraph}{ }{5}%
\contentsline {chapter}{\numberline {2}Конструкторская часть}{6}%
\contentsline {paragraph}{ }{6}%
\contentsline {section}{\numberline {2.1}Требования к программе}{6}%
\contentsline {section}{\numberline {2.2}Схемы алгоритмов}{6}%
\contentsline {paragraph}{ }{6}%
\contentsline {paragraph}{ }{11}%
\contentsline {chapter}{\numberline {3}Технологическая часть}{12}%
\contentsline {paragraph}{ }{12}%
\contentsline {section}{\numberline {3.1}Выбор ЯП}{12}%
\contentsline {paragraph}{ }{12}%
\contentsline {section}{\numberline {3.2}Листинг кода алгоритмов}{12}%
\contentsline {paragraph}{ }{12}%
\contentsline {section}{\numberline {3.3}Тестирование разработанных функций}{14}%
\contentsline {paragraph}{ }{14}%
\contentsline {paragraph}{}{14}%
\contentsline {paragraph}{}{17}%
\contentsline {section}{\numberline {3.4}Вывод}{18}%
\contentsline {paragraph}{ }{18}%
\contentsline {chapter}{\numberline {4}Исследовательская часть}{19}%
\contentsline {paragraph}{ }{19}%
\contentsline {section}{\numberline {4.1}Сравнительный анализ на основе замеров времени}{19}%
\contentsline {paragraph}{ }{19}%
\contentsline {paragraph}{ }{21}%
\contentsline {section}{Вывод}{21}%
\contentsline {paragraph}{ }{21}%
\contentsline {chapter}{Заключение}{22}%
\contentsline {paragraph}{ }{22}%
\contentsline {chapter}{Список литературы}{22}%
