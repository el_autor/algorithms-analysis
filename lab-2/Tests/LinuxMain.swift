import XCTest

import lab_2Tests

var tests = [XCTestCaseEntry]()
tests += lab_2Tests.allTests()
XCTMain(tests)
