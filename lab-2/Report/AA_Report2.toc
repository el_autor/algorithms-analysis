\babel@toc {russian}{}
\contentsline {chapter}{Введение}{2}%
\contentsline {chapter}{\numberline {1}Аналитическая часть}{3}%
\contentsline {section}{\numberline {1.1}Алгоритм Винограда}{3}%
\contentsline {section}{\numberline {1.2}Вывод}{4}%
\contentsline {chapter}{\numberline {2}Конструкторская часть}{5}%
\contentsline {section}{\numberline {2.1}Схемы алгоритмов}{5}%
\contentsline {chapter}{\numberline {3}Технологическая часть}{9}%
\contentsline {section}{\numberline {3.1}Выбор ЯП}{9}%
\contentsline {section}{\numberline {3.2}Листинг кода алгоритмов}{9}%
\contentsline {subsection}{\numberline {3.2.1}Оптимизация алгоритма Винограда}{13}%
\contentsline {section}{\numberline {3.3}Вывод}{15}%
\contentsline {chapter}{\numberline {4}Исследовательская часть}{16}%
\contentsline {section}{\numberline {4.1}Время работы алгоритмов}{16}%
\contentsline {section}{\numberline {4.2}Вывод}{17}%
\contentsline {chapter}{Заключение}{18}%
\contentsline {chapter}{Список литературы}{18}%
