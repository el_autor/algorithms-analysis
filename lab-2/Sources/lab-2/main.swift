import Darwin

let intSize = 8

func greeting() {
    let text = """
    Программа для умножения матриц стандартным алгоритмом и алгоритмом Винограда.
    """
    print(text)
}

func showMenu() {
    let text = """
    Набор команд:
    1 - ввести матрицу A
    2 - ввести матрицу B
    3 - стандартный алгоритм
    4 - алгоритм Винограда
    5 - алгоритм Винограда с оптимизацией
    6 - показать меню
    7 - анализ временных затрат
    8 - тестирование
    9 - выйти
    """
    print(text)
}

func dataIsValid(_ a: Array<Array<Int>>, _ b: Array<Array<Int>>) -> Bool {
    guard a[0].count == b.count else { 
        print("Размерность матриц не допускает умножения!")
        return false
    }
    
    return true
}

func showMatrix(_ matrix: Array<Array<Int>>) {
    for i in 0..<matrix.count {
        for j in 0..<matrix[i].count {
            print(matrix[i][j], terminator: " ")
        }

        print()
    }
}

func inputMatrix(_ matrix:inout Array<Array<Int>>) {
    matrix = []
    print("Введите кол-во строк")
    let a = Int(readLine()!)!
    print("Введите кол-во столбцов")
    let b = Int(readLine()!)!

    for _ in 0..<a {
        var row = [Int]()
        
        _ = readLine()?.split(separator: " ").map {
            if let x = Int($0) {
                row.append(x)
            }
            else {
                print("Неверный ввод")
            }
        }

        if (row.count != b) {
            print("Введено неверное кол-во элементов в строке")
        }

        matrix.append(row)
    }
}

var a: Array<Array<Int>> = []
var b: Array<Array<Int>> = []
var loop = true

greeting()
showMenu()

while loop {
    let input = readLine()!
    let commandNumber = Int(input)
   
    let command = Commands(rawValue: commandNumber! - 1) 

    switch command {
    case .inputMatrixA:
        inputMatrix(&a)
    case .inputMatrixB:
        inputMatrix(&b)
    case .standartAlgorithm:
        if !dataIsValid(a, b) {
            loop = false
            break
        }

        let start = mach_absolute_time()
        let result = standartAlgorithm(a, b)                 
        let end = mach_absolute_time()
        
        print("Результат:")
        showMatrix(result)
        print("Процессорное время(тики):", end - start)
        print("Память ~", a[0].count * a.count * intSize + b[0].count * b.count * intSize, "байт")
    case .vinograd:
        if !dataIsValid(a, b) {
            loop = false
            break
        }

        let start = mach_absolute_time()
        let result = vinograd(a, b)                 
        let end = mach_absolute_time()

        print("Результат:")
        showMatrix(result)
        print("Время в тиках:", end - start)
        print("Память ~", a[0].count * a.count * intSize + b[0].count * b.count * intSize, "байт")
    case .optimizedVinograd:
        if !dataIsValid(a, b) {
            loop = false
            break
        }

        let start = mach_absolute_time()
        let result = optimizedVinograd(a, b)
        let end = mach_absolute_time()

        print("Результат:")
        showMatrix(result)
        print("Процессорное время(тики):", end - start)
        print("Память ~", a[0].count * a.count * intSize + b[0].count * b.count * intSize, "байт")
    case .showMenu:
        showMenu()
    case .timeAnalyze:
        analyzeTime()
    case .testing:
        runTests()
    case .exit:
        loop = false
    default:
        ()
    }
}