enum Commands: Int {
    case inputMatrixA
    case inputMatrixB
    case standartAlgorithm
    case vinograd
    case optimizedVinograd
    case showMenu
    case timeAnalyze
    case testing
    case exit
}