import Darwin

func analyze(_ m1: Array<Array<Int>>, _ m2: Array<Array<Int>>, _ timesToRepeat: UInt64) {
    var start: UInt64!
    var end: UInt64!

    print("\nРазмерность матриц = \(m1.count) * \(m1[0].count)")

    start = mach_absolute_time()
    for _ in 0..<timesToRepeat {
        let _ = standartAlgorithm(m1, m2)             
    }
    end = mach_absolute_time()

    print("Стандартный алгоритм ~", (end - start) / timesToRepeat)

    start = mach_absolute_time()
    for _ in 0..<timesToRepeat {
        let _ = vinograd(m1, m2)           
    }
    end = mach_absolute_time()

    print("Алгоритм Винограда ~", (end - start) / timesToRepeat)

    start = mach_absolute_time()
    for _ in 0..<timesToRepeat {
        let _ = optimizedVinograd(m1, m2)           
    }
    end = mach_absolute_time()

    print("Оптимизированный алгоритм Винограда ~", (end - start) / timesToRepeat)
}

func generateRandomMatrix(_ rows: Int, _ cols: Int) -> Array<Array<Int>> {
    var result: Array<Array<Int>> = []

    for _ in 0..<rows {
        var row = [Int]()

        for _ in 0..<cols {
            row.append(Int.random(in: -5...5))
        }

        result.append(row)
    }

    return result
}

func analyzeTime() {
    var m1 = generateRandomMatrix(1, 1)
    var m2 = generateRandomMatrix(1, 1)
    analyze(m1, m2, 10000)

    m1 = generateRandomMatrix(3, 3)
    m2 = generateRandomMatrix(3, 3)
    analyze(m1, m2, 2500)

    m1 = generateRandomMatrix(5, 5)
    m2 = generateRandomMatrix(5, 5)
    analyze(m1, m2, 500)

    m1 = generateRandomMatrix(10, 10)
    m2 = generateRandomMatrix(10, 10)
    analyze(m1, m2, 100)

    m1 = generateRandomMatrix(50, 50)
    m2 = generateRandomMatrix(50, 50)
    analyze(m1, m2, 50)

    m1 = generateRandomMatrix(100, 100)
    m2 = generateRandomMatrix(100, 100)
    analyze(m1, m2, 10)    

    m1 = generateRandomMatrix(250, 250)
    m2 = generateRandomMatrix(250, 250)
    analyze(m1, m2, 5)

    m1 = generateRandomMatrix(500, 500)
    m2 = generateRandomMatrix(500, 500)
    analyze(m1, m2, 1)
}