func testReport(_ number: Int, _ resultValue: Array<Array<Int>>, _ properValue: Array<Array<Int>>) {
    print("Тест \(number) не пройден!")
    print("Полученный результат:")
    showMatrix(resultValue)
    print("Ожидаемый результат:")
    showMatrix(properValue)    
}

func test(_ m1: Array<Array<Int>>, _ m2: Array<Array<Int>>, _ properValue: Array<Array<Int>>, _ testNumber: Int) {
    var resultValue: Array<Array<Int>>!

    resultValue = standartAlgorithm(m1, m2) 
    guard resultValue == properValue else {
        print("Стандартный алгоритм")
        testReport(testNumber, resultValue, properValue)
        return 
    }

    resultValue = vinograd(m1, m2)
    guard resultValue == properValue else {
        print("Алгоритм Винограда")
        testReport(testNumber, resultValue, properValue)
        return 
    }

    resultValue = optimizedVinograd(m1, m2)
    guard resultValue == properValue else {
        print("Оптимизированный алгоритм Винограда")
        testReport(testNumber, resultValue, properValue)
        return 
    }

    print("Тест \(testNumber) пройден успешно")
}   

func runTests() {
    test([[1]], [[1]], [[1]], 1)

    test([[1, 1],
          [1, 1]], 
         [[2, 2],
          [2, 2]],
         [[4, 4],
          [4, 4]]
          , 2)

    test([[1, 1],
          [1, 1],
          [1, 1]],
         [[2, 2, 2],
          [2, 2, 2]], 
         [[4, 4, 4],
          [4, 4, 4],
          [4, 4, 4]], 3)
    
    test([[1, 1, 1],
          [1, 1, 1]],
         [[2, 2],
          [2, 2],
          [2, 2]],
         [[6, 6],
          [6, 6]], 4)
}
