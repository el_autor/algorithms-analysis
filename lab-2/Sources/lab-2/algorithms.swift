func createEmpty(_ rows: Int, _ cols: Int) -> Array<Array<Int>>{
    var matrix: Array<Array<Int>> = []

    for _ in 0..<rows {
        matrix.append(Array(repeating: 0, count: cols))
    }

    return matrix
}

func standartAlgorithm(_ a: Array<Array<Int>>, _ b: Array<Array<Int>>) -> Array<Array<Int>> {
    var result = createEmpty(b[0].count, a.count)

    let aRows = a.count
    let bCols = b[0].count
    let aCols = a[0].count

    for i in 0..<aRows {
        for j in 0..<bCols {
            var value = 0
            for k in 0..<aCols {
                value += a[i][k] * b[k][j]
            }
            result[i][j] = value
        }
    }

    return result
}

func vinograd(_ a: Array<Array<Int>>, _ b: Array<Array<Int>>) -> Array<Array<Int>> {
    let rowsA = a.count
    let rowsB = b.count

    let colsA = a[0].count
    let colsB = b[0].count

    var constsH: [Int] = []
    var constsV: [Int] = []

    var result = createEmpty(colsB, rowsA)

    for i in 0..<rowsA {
        var value = 0
        
        for j in 0..<(colsA / 2) {
            value = value + a[i][j * 2] * a[i][j * 2 + 1]
        }

        constsH.append(value)
    }

    for i in 0..<colsB {
        var value = 0
        
        for j in 0..<(rowsB / 2) {
            value = value + b[j * 2][i] * b[j * 2 + 1][i]
        }

        constsV.append(value)
    }
    
    for i in 0..<rowsA {
        for j in 0..<colsB {
            result[i][j] = -constsH[i] - constsV[j]

            for k in 0..<(colsA / 2) {
                result[i][j] = result[i][j] + (a[i][2 * k + 1] + b[2 * k][j]) * (a[i][2 * k] + b[2 * k + 1][j])
            }
        }
    }

    if colsA % 2 == 1 {
        for i in 0..<rowsA {    
            for j in 0..<colsB {
                result[i][j] = result[i][j] + a[i][colsA - 1] * b[colsA - 1][j]
            }
        }
    }

    return result
}

func optimizedVinograd(_ a: Array<Array<Int>>, _ b: Array<Array<Int>>) -> Array<Array<Int>> {
    let rowsA = a.count
    let rowsB = b.count

    let colsA = a[0].count
    let colsB = b[0].count

    var constsH: [Int] = []
    var constsV: [Int] = []

    var result = createEmpty(colsB, rowsA)

    let colsAMod = colsA % 2
    let rowsBMod = rowsB % 2

    for i in 0..<rowsA {
        var value = 0
        
        for j in stride(from: 0, to: colsA - colsAMod, by: 2) {
            value += a[i][j] * a[i][j + 1]
        }

        constsH.append(value)
    }

    for i in 0..<colsB {
        var value = 0
        
        for j in stride(from: 0, to: rowsB - rowsBMod, by: 2) {
            value += b[j][i] * b[j + 1][i]
        }

        constsV.append(value)
    }
    
    for i in 0..<rowsA {
        for j in 0..<colsB {
            var buffer = -constsH[i] - constsV[j]

            for k in stride(from: 0, to: colsA - colsAMod, by: 2) {
                buffer += (a[i][k + 1] + b[k][j]) * (a[i][k] + b[k + 1][j])
            }

            result[i][j] = buffer
        }
    }

    if colsAMod == 1 {
        let premaxCols = colsA - 1
        for i in 0..<rowsA {
            for j in 0..<colsB {
                result[i][j] += a[i][premaxCols] * b[premaxCols][j]
            }
        }
    }

    return result
}