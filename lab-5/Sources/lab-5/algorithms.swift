import Foundation

struct Element {
    var index: Int
}

@available(macOS 10.12, *)
func conveyorProcessing(conveyorNumber: Int, sleepTime: UInt32) {
    let lock = NSLock()

    var conveyorMatrix: Array<Array<Element>> = Array<Array<Element>>(repeating: [], count: conveyorNumber)

    for i in 0...5 {
        conveyorMatrix[0].insert(Element(index: i), at: 0)
    }

    // Завершающий елемент
    conveyorMatrix[0].insert(Element(index: -1), at: 0)
    var threads: [Thread] = []

    let startConveyor = Thread {
        var flag = true

        while flag {
            let elem = conveyorMatrix[0].removeLast()
            print("индекс - \(elem.index)", "конвейер - 1")
            sleep(sleepTime)
            lock.lock()
            conveyorMatrix[1].insert(elem, at: 0)
            lock.unlock()

            if elem.index == -1 {
                flag = false
            }
        }
    }

    threads.append(startConveyor)

    for i in 1..<conveyorNumber - 1 {
        let conveyor = Thread {
            var flag = true

            while flag {
                if conveyorMatrix[i].count != 0 {
                    lock.lock()
                    let elem = conveyorMatrix[i].removeLast()
                    lock.unlock()

                    print("индекс - \(elem.index)", "конвейер - \(i + 1)")
                    sleep(sleepTime)

                    lock.lock()
                    conveyorMatrix[i + 1].insert(elem, at: 0)
                    lock.unlock()

                    if (elem.index == -1) {
                        flag = false
                    }
                }
            }
        }        

        threads.append(conveyor)
    }

    let lastConveyor = Thread {
        var flag = true

        while flag {
            if conveyorMatrix[conveyorNumber - 1].count != 0 {
                lock.lock()
                let elem = conveyorMatrix[conveyorNumber - 1].removeLast()
                lock.unlock()

                print("индекс - \(elem.index)", "конвейер - \(conveyorNumber)")
                sleep(sleepTime)

                if (elem.index == -1) {
                    flag = false
                }
            }
        }
    }

    threads.append(lastConveyor)

    for i in 0..<threads.count {
        threads[i].start()
    }

    var flag = true

    while (flag) {
        flag = false

        for i in 0..<threads.count {
            if !threads[i].isFinished {
                flag = true
            }
        }
    }
}

func linearProcessing(conveyorNumber: Int, sleepTime: UInt32) {
    var conveyorMatrix: Array<Array<Element>> = Array<Array<Element>>(repeating: [], count: conveyorNumber)

    for i in 0...5 {
        conveyorMatrix[0].insert(Element(index: i), at: 0)
    }

    // Завершающий елемент
    conveyorMatrix[0].insert(Element(index: -1), at: 0)

    for i in 0..<conveyorNumber - 1 {
        var flag = true

        while flag {
            let elem = conveyorMatrix[i].removeLast()
            print("индекс - \(elem.index)", "конвейер - \(i + 1)")
            sleep(sleepTime)
            conveyorMatrix[i + 1].insert(elem, at: 0)

            if elem.index == -1 {
                flag = false
            }
        }
    }

    var flag = true

    while flag {
        let elem = conveyorMatrix[conveyorNumber - 1].removeLast()

        print("индекс - \(elem.index)", "конвейер - \(conveyorNumber)")
        sleep(sleepTime)

        if (elem.index == -1) {
            flag = false
        }
    }
}