import Darwin

let intSize = 8

func greeting() {
    let text = """
    Программа для имитации конвейерной обработки
    """
    print(text)
}

func showMenu() {
    let text = """
    Набор команд:
    1 - линейная обработка
    2 - конвейерная обработка
    3 - показать меню
    4 - выйти
    """
    print(text)
}

var loop = true

greeting()
showMenu()

while loop {
    let input = readLine()!
    let commandNumber = Int(input)
   
    let command = Commands(rawValue: commandNumber! - 1) 

    switch command {
    case .linear:
        print("Введите кол-во конвейеров:")
        let conveyorNumber = Int(readLine()!)!

        print("Введите задержку:")
        let sleepTime = UInt32(readLine()!)!

        let start = mach_absolute_time()
        linearProcessing(conveyorNumber: conveyorNumber, sleepTime: sleepTime)
        let end = mach_absolute_time()

        print("Результат:")
        print("Процессорное время(тики):", end - start)
    case .conveyor:
        print("Введите кол-во конвейеров:")
        let conveyorNumber = Int(readLine()!)!

        print("Введите задержку:")
        let sleepTime = UInt32(readLine()!)!

        if #available(macOS 10.12, *) {
            let start = mach_absolute_time()      
            conveyorProcessing(conveyorNumber: conveyorNumber, sleepTime: sleepTime)
            let end = mach_absolute_time()

            print("Результат:")
            print("Процессорное время(тики):", end - start)
        }
    case .showMenu:
        showMenu()
    case .exit:
        loop = false
    default:
        ()
    }
}