import XCTest

import lab_5Tests

var tests = [XCTestCaseEntry]()
tests += lab_5Tests.allTests()
XCTMain(tests)
