\babel@toc {russian}{}
\contentsline {chapter}{Введение}{2}%
\contentsline {paragraph}{}{2}%
\contentsline {paragraph}{}{2}%
\contentsline {chapter}{\numberline {1}Аналитическая часть}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {section}{\numberline {1.1}Общие сведения о конвейерной обработке}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {section}{\numberline {1.2}Параллельное программирование}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {paragraph}{ }{4}%
\contentsline {paragraph}{ }{4}%
\contentsline {paragraph}{ }{4}%
\contentsline {subsection}{\numberline {1.2.1}Организация взаимодействия параллельных потоков}{4}%
\contentsline {paragraph}{ }{4}%
\contentsline {section}{\numberline {1.3}Вывод}{5}%
\contentsline {paragraph}{ }{5}%
\contentsline {chapter}{\numberline {2}Конструкторская часть}{6}%
\contentsline {paragraph}{ }{6}%
\contentsline {section}{\numberline {2.1}Основные требования к программе}{6}%
\contentsline {section}{\numberline {2.2}Организация обработки}{6}%
\contentsline {paragraph}{ }{6}%
\contentsline {section}{\numberline {2.3}Вывод}{7}%
\contentsline {paragraph}{ }{7}%
\contentsline {chapter}{\numberline {3}Технологическая часть}{8}%
\contentsline {paragraph}{ }{8}%
\contentsline {section}{\numberline {3.1}Выбор ЯП}{8}%
\contentsline {paragraph}{ }{8}%
\contentsline {section}{\numberline {3.2}Листинг кода алгоритмов}{8}%
\contentsline {paragraph}{ }{8}%
\contentsline {section}{\numberline {3.3}Тестирование разработанных функций}{12}%
\contentsline {paragraph}{ }{12}%
\contentsline {paragraph}{}{13}%
\contentsline {section}{\numberline {3.4}Вывод}{14}%
\contentsline {chapter}{\numberline {4}Исследовательская часть}{15}%
\contentsline {paragraph}{ }{15}%
\contentsline {section}{\numberline {4.1}Сравнительный анализ на основе замеров времени}{15}%
\contentsline {paragraph}{ }{15}%
\contentsline {section}{\numberline {4.2}Вывод}{16}%
\contentsline {paragraph}{ }{16}%
\contentsline {chapter}{Заключение}{17}%
\contentsline {paragraph}{ }{17}%
\contentsline {paragraph}{ }{17}%
\contentsline {paragraph}{ }{17}%
\contentsline {chapter}{Список литературы}{17}%
