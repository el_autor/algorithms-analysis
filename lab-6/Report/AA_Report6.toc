\babel@toc {russian}{}
\contentsline {chapter}{Введение}{2}%
\contentsline {paragraph}{ }{2}%
\contentsline {chapter}{\numberline {1}Аналитическая часть}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {section}{\numberline {1.1}Постановка задачи}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {section}{\numberline {1.2}Задача коммивояжера}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {section}{\numberline {1.3}Решение полным перебором}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {section}{\numberline {1.4}Муравьиные алгоритмы}{4}%
\contentsline {paragraph}{ }{4}%
\contentsline {section}{\numberline {1.5}Муравьиный алгоритм в задаче коммивояжера}{7}%
\contentsline {paragraph}{ }{7}%
\contentsline {section}{Введение}{8}%
\contentsline {paragraph}{ }{8}%
\contentsline {chapter}{\numberline {2}Конструкторская часть}{9}%
\contentsline {paragraph}{ }{9}%
\contentsline {section}{\numberline {2.1}Требования к программе}{9}%
\contentsline {section}{\numberline {2.2}Схемы алгоритмов}{9}%
\contentsline {paragraph}{ }{9}%
\contentsline {section}{Вывод}{12}%
\contentsline {paragraph}{ }{12}%
\contentsline {chapter}{\numberline {3}Технологическая часть}{13}%
\contentsline {paragraph}{ }{13}%
\contentsline {section}{\numberline {3.1}Выбор ЯП}{13}%
\contentsline {paragraph}{ }{13}%
\contentsline {section}{\numberline {3.2}Листинг кода алгоритмов}{13}%
\contentsline {paragraph}{ }{13}%
\contentsline {section}{\numberline {3.3}Тестирование разработанных функций}{17}%
\contentsline {paragraph}{ }{17}%
\contentsline {paragraph}{}{17}%
\contentsline {section}{\numberline {3.4}Вывод}{17}%
\contentsline {paragraph}{ }{17}%
\contentsline {paragraph}{}{19}%
\contentsline {chapter}{\numberline {4}Исследовательская часть}{20}%
\contentsline {paragraph}{ }{20}%
\contentsline {section}{\numberline {4.1}Сравнительный анализ на основе замеров времени}{20}%
\contentsline {paragraph}{ }{20}%
\contentsline {section}{\numberline {4.2}Параметризация муравьиного алгоритма}{21}%
\contentsline {paragraph}{ }{21}%
\contentsline {section}{Вывод}{22}%
\contentsline {paragraph}{ }{22}%
\contentsline {chapter}{Заключение}{23}%
\contentsline {paragraph}{ }{23}%
\contentsline {chapter}{Список литературы}{23}%
