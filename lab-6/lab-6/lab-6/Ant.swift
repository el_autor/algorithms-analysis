class Ant {
    var visitedCities: [Int] = []
    
    func visitCity(cityId: Int) {
        visitedCities.append(cityId)
    }
}
