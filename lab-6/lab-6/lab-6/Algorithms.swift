import Foundation

var permutations: Array<Array<Int>> = []

func nextElem(permutation: [Int], startIndex: Int, maxIndex: Int) {
    if startIndex == maxIndex {
        permutations.append(permutation)
        return
    }
    
    var notUsed: [Int] = []
    
    for i in .zero + 1..<maxIndex {
        if !permutation.contains(i) {
            notUsed.append(i)
        }
    }
    
    for _ in startIndex..<maxIndex {
        var permutationCopy = permutation
        
        permutationCopy.append(notUsed.removeFirst())
        
        nextElem(permutation: permutationCopy, startIndex: startIndex + 1,  maxIndex: maxIndex)
    }
}

func getAllPermutations(maxNumber: Int) -> Array<Array<Int>> {
    permutations = []
   
    for i in .zero + 1..<maxNumber {
        var permutation: [Int] = [0]
        
        permutation.append(i)
        nextElem(permutation: permutation, startIndex: 2,  maxIndex: maxNumber)
    }
    
    return permutations
}

func bruteForce(graph: Array<Array<Int?>>) -> (Int, [Int]) {
    var minPath = Int.max
    var minPermutation = [Int]()
    
    let allPermutations = getAllPermutations(maxNumber: graph.count)
    
    allPermutations.forEach { permutation in
        var permutationCopy = permutation
        var currentPath = 0
        
        permutationCopy.append(permutation[0])
        
        for i in .zero..<graph.count {
            guard let path = graph[permutationCopy[i]][permutationCopy[i + 1]] else {
                return
            }
            
            currentPath += path
        }
        
        if currentPath < minPath {
            minPermutation = permutation
            minPath = currentPath
        }
    }
    
    return (minPath, minPermutation)
}

func createSquareMatrix(size: Int, value: Double) -> Array<Array<Double>> {
    var matrix = Array<Array<Double>>()
    
    for _ in .zero..<size {
        matrix.append(Array<Double>(repeating: value, count: size))
    }
    
    return matrix
}

func createAnts(count: Int) -> [Ant] {
    var ants = Array<Ant>()
    
    for _ in .zero..<count {
        let ant = Ant()
        
        ant.visitCity(cityId: .zero)
        ants.append(ant)
    }
    
    return ants
}

func antAlgorithm(graph: Array<Array<Int?>>,
                  iters: Int,
                  alpha: Double,
                  beta: Double,
                  q: Double,
                  ro: Double) -> Int {
    var minPath = Int.max
    let totalAnts = graph.count
    
    var pheromones = createSquareMatrix(size: totalAnts, value: 0.1)
    
    // Проходим по дням
    for _ in .zero..<iters {
        let ants = createAnts(count: totalAnts)
        
        // Построение маршрута колонии
        for _ in .zero..<graph.count - 1 {
            var tempPheromones = createSquareMatrix(size: totalAnts, value: .zero)
            
            // Цикл по муравьям
            for j in .zero..<totalAnts {
                var sumChance: Double = 0, chance: Double = 0
                let ant = ants[j]
                let curCity = ant.visitedCities.last!
                
                for cityId in .zero..<graph.count {
                    if !ant.visitedCities.contains(cityId) {
                        sumChance += pow(pheromones[curCity][cityId], alpha) * pow(1 / Double(graph[curCity][cityId] ?? Int.max), beta)
                    }
                }
                
                var x: Double = Double.random(in: .zero...1)
                var k = 0
                
                while x > 0 {
                    if !ant.visitedCities.contains(k) {
                        chance = pow(pheromones[curCity][k], alpha) * pow(1 / Double(graph[curCity][k] ?? Int.max), beta)
                        chance /= sumChance
                        x -= chance
                    }
                    
                    k += 1
                }
                
                k -= 1
                ants[j].visitCity(cityId: k)
                tempPheromones[curCity][k] += q / Double(graph[curCity][k] ?? Int.max)
            }
            
            for ii in .zero..<graph.count {
                for j in .zero..<graph.count {
                    pheromones[ii][j] = (1 - ro) * pheromones[ii][j] + tempPheromones[ii][j]
                }
            }
        }
        
        for ant in ants {
            ant.visitCity(cityId: .zero)
            let path = ant.visitedCities
            var currentPath = 0
            
            for i in .zero..<graph.count {
                guard let pathLength = graph[path[i]][path[i + 1]] else {
                    continue
                }
                
                currentPath += pathLength
            }
            
            if currentPath < minPath {
                minPath = currentPath
            }
        }
    }
    
    return minPath
}
