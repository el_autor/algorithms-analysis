import Darwin

func greeting() {
    let text = """
    Программа для решения задачи коммивояжера
    """
    print(text)
}

func showMenu() {
    let text = """
    Набор команд:
    1 - ввести матрицу смежности
    2 - полный перебор
    3 - муравьиный алгоритм
    4 - показать меню
    5 - выйти
    """
    print(text)
}

func inputGraph() -> Array<Array<Int?>> {
    print("Введите кол-во вершин:")
    let vertexNumber = Int(readLine()!)!
    
    var graph = Array<Array<Int?>>()
    
    for _ in .zero..<vertexNumber {
        graph.append(Array<Int?>(repeating: nil, count: vertexNumber))
    }
    
    print("Задайте связи: <Начало> <Конец> <Длина пути>. Чтобы закончить введите -1.")
    
    while true {
        let data = readLine()!.split(separator: " ").map{Int($0)!}
        
        if (data[0] == -1) {
            break
        }
        
        graph[data[0] - 1][data[1] - 1] = data[2]
        graph[data[1] - 1][data[0] - 1] = data[2]
    }
    
    return graph
}

var loop = true
var graph: Array<Array<Int?>> = []

greeting()
showMenu()

while loop {
    let input = readLine()!
    let commandNumber = Int(input)!
   
    let command = Commands(rawValue: commandNumber - 1)

    switch command {
    case .input:
        graph = inputGraph()
    case .bruteForce:
        let start = mach_absolute_time()
        let path = bruteForce(graph: graph).0
        let end = mach_absolute_time()

        print("Результат - ", path)
        print("Процессорное время(тики):", end - start)
    case .ant:
        print("Кол-во дней:")
        let iters = Int(readLine()!)!
        
        print("Параметр влияния длины пути:")
        let alpha = Double(readLine()!)!
        
        print("Параметр влияния феромона:")
        let beta = Double(readLine()!)!
        
        print("Кол-во феромона, переносимого муравьем:")
        let q = Double(readLine()!)!
        
        print("Коеффициент испарения феромона:")
        let ro = Double(readLine()!)!
        
        let start = mach_absolute_time()
        let path = antAlgorithm(graph: graph,
                                iters: iters,
                                alpha: alpha,
                                beta: beta,
                                q: q,
                                ro: ro)
        let end = mach_absolute_time()

        print("Результат - ", path)
        print("Процессорное время(тики):", end - start)
    case .menu:
        showMenu()
    case .exit:
        loop = false
    default:
        ()
    }
}
