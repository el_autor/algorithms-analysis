enum Commands: Int {
    case generateRandom
    case generateOrdered
    case generateBackOrdered
    case handInput
    case showCurrentArray
    case bubbleSort
    case insertionSort
    case quickSort
    case showMenu
    case exit
}