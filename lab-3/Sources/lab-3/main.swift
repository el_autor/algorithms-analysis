import Darwin

let sizeInt = 8

func greeting() {
    let text = """
    Программа для сравнения алгоритмов сортировки.
    """
    print(text)
}

func showMenu() {
    let text = """
    Набор команд:
    1 - сгенерировать случайный массив
    2 - сгенерировать упорядоченный массив
    3 - сгенерировать обратно упорядоченный массив
    4 - ввести массив
    5 - показать текущий массив
    6 - сортировка пузырьком
    7 - сортировка вставками
    8 - быстрая сортировка
    9 - показать меню
    10 - выйти
    """
    print(text)
}

func createRandom(_ array: inout Array<Int>) {
    array = []

    print("Введите кол-во элементов:")
    let count = Int(readLine()!)!

    print("Введите нижнюю границу диапазона:")
    let downBound = Int(readLine()!)!

    print("Введите верхнюю границу диапазона:")
    let upBound = Int(readLine()!)!

    for _ in 0..<count {
        array.append(Int.random(in: downBound...upBound))
    }
}

func createOrdered(_ array: inout Array<Int>) {
    array = []

    print("Введите кол-во элементов:")
    let count = Int(readLine()!)!

    print("Введите нижнюю границу диапазона:")
    var downBound = Int(readLine()!)!

    for _ in 0..<count {
        array.append(downBound)
        downBound += 1
    }
}

func createBackOrdered(_ array: inout Array<Int>) {
    array = []

    print("Введите кол-во элементов:")
    let count = Int(readLine()!)!

    print("Введите верхнюю границу диапазона:")
    var upBound = Int(readLine()!)!

    for _ in 0..<count {
        array.append(upBound)
        upBound -= 1
    }
}

func consoleInput(_ array: inout Array<Int>) {
    array = []

    print("Введите элементы массива:")
    _ = readLine()?.split(separator: " ").map {
            if let x = Int($0) {
                array.append(x)
            }
            else {
                print("Неверный ввод")
            }
        }
}

func showArray(_ array: Array<Int>) {
    guard array.count <= 100 else {
        print("Размер массива займет много места(больше 100 элементов)")
        return
    }

    for i in 0..<array.count {
        print(array[i], terminator: " ")
    }

    print()
}

var array: Array<Int> = []
var arrayCopy: Array<Int> = []
var loop = true

greeting()
showMenu()

while loop {
    let commandNumber = Int(readLine()!)
   
    let command = Commands(rawValue: commandNumber! - 1)

    switch command {
    case .generateRandom:
        createRandom(&array)
    case .generateOrdered:
        createOrdered(&array)
    case .generateBackOrdered:
        createBackOrdered(&array)
    case .handInput:
        consoleInput(&array)
    case .showCurrentArray:
        showArray(array)
    case .bubbleSort:
        arrayCopy = array   
        let start = mach_absolute_time()
        bubbleSort(&arrayCopy)                 
        let end = mach_absolute_time()
        
        print("Результат:")
        showArray(arrayCopy)
        print("Процессорное время(тики):", end - start)
        print("Память ~", array.count * sizeInt, "байт")
    case .insertionSort:
        arrayCopy = array
        let start = mach_absolute_time()
        insertionSort(&arrayCopy)                 
        let end = mach_absolute_time()

        print("Результат:")
        showArray(arrayCopy)
        print("Время в тиках:", end - start)
        print("Память ~", array.count * sizeInt, "байт")
    case .quickSort:
        arrayCopy = array
        let start = mach_absolute_time()
        arrayCopy.sort{$0 < $1}
        let end = mach_absolute_time()

        print("Результат:")
        showArray(arrayCopy)
        print("Процессорное время(тики):", end - start)
        print("Память ~", array.count * sizeInt, "байт")
    case .showMenu:
        showMenu()
    case .exit:
        loop = false
    default:
        ()
    }
}