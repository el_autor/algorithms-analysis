func bubbleSort(_ array: inout Array<Int>) {
    var i = 0
    var didSwapped = false

    while (i < array.count) {
        if (i + 1 != array.count && array[i] > array[i + 1]) {
            let temp = array[i]
            array[i] = array[i + 1]
            array[i + 1] = temp

            didSwapped = true
        }

        i += 1

        if (i == array.count && didSwapped) {
            didSwapped = false
            i = 0
        }
    }
}

func insertionSort(_ array: inout Array<Int>) {
    var key: Int = 0
    var j: Int = 0

    for i in 1..<array.count {
        key = array[i]
        j = i - 1

        while (j >= 0 && array[j] > key) {
            array[j + 1] = array[j]
            j -= 1
        }

        array[j + 1] = key
    }
}