import XCTest

import lab_3Tests

var tests = [XCTestCaseEntry]()
tests += lab_3Tests.allTests()
XCTMain(tests)
