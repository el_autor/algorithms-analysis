\babel@toc {russian}{}
\contentsline {chapter}{Введение}{2}%
\contentsline {paragraph}{ }{2}%
\contentsline {chapter}{\numberline {1}Аналитическая часть}{3}%
\contentsline {section}{\numberline {1.1}Описание алгоритмов}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {subsection}{\numberline {1.1.1}Сортировка пузырьком}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {subsection}{\numberline {1.1.2}Сортировка вставками}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {subsection}{\numberline {1.1.3}Быстрая сортировка}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {chapter}{\numberline {2}Конструкторская часть}{5}%
\contentsline {section}{\numberline {2.1}Схемы алгоритмов}{5}%
\contentsline {paragraph}{ }{5}%
\contentsline {section}{\numberline {2.2}Трудоемкость алгоритмов}{7}%
\contentsline {subsection}{\numberline {2.2.1}Сортировка пузырьком с флагом}{8}%
\contentsline {subsection}{\numberline {2.2.2}Сортировка вставками}{9}%
\contentsline {subsection}{\numberline {2.2.3}Быстрая сортировка}{9}%
\contentsline {section}{\numberline {2.3}Вывод}{10}%
\contentsline {chapter}{\numberline {3}Технологическая часть}{11}%
\contentsline {section}{\numberline {3.1}Выбор ЯП}{11}%
\contentsline {paragraph}{ }{11}%
\contentsline {section}{\numberline {3.2}Листинг кода}{11}%
\contentsline {paragraph}{ }{11}%
\contentsline {section}{\numberline {3.3}Вывод}{12}%
\contentsline {paragraph}{ }{12}%
\contentsline {chapter}{\numberline {4}Исследовательская часть}{13}%
\contentsline {paragraph}{ }{13}%
\contentsline {section}{\numberline {4.1}Время работы алгоритмов}{13}%
\contentsline {paragraph}{ }{13}%
\contentsline {section}{\numberline {4.2}Вывод}{14}%
\contentsline {paragraph}{ }{14}%
\contentsline {paragraph}{ }{16}%
\contentsline {chapter}{Список литературы}{17}%
