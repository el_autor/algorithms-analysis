import Foundation

func createEmpty(_ rows: Int, _ cols: Int) -> Array<Array<Int>>{
    var matrix: Array<Array<Int>> = []

    for _ in 0..<rows {
        matrix.append(Array(repeating: 0, count: cols))
    }

    return matrix
}

@available(macOS 10.12, *)
func standartAlgorithm(_ a: Array<Array<Int>>, _ b: Array<Array<Int>>, threadNumber: Int) -> Array<Array<Int>> {
    var result = createEmpty(b[0].count, a.count)
    let lock = NSLock()

    let aRows = a.count
    let bCols = b[0].count
    let aCols = a[0].count

    var threads: [Thread] = []
    for curThreadNumber in 0..<threadNumber {
        threads.append(Thread{
            for i in stride(from: curThreadNumber, to: aRows, by: threadNumber) {
                for j in 0..<bCols {
                    var value = 0
                    
                    for k in 0..<aCols {
                        value += a[i][k] * b[k][j]
                    }

                    lock.lock()
                    result[i][j] = value
                    lock.unlock()
                }
            }
        })
        
        threads[curThreadNumber].start()
    }

    var tasksFinished = false

    while (!tasksFinished) {
        var flag = false

        for tNum in 0..<threadNumber {
            if (!threads[tNum].isFinished) {
                flag = true
                break
            }
        }

        if !flag {
            tasksFinished = true
        }
    }

    return result
}

func standartAlgorithm(_ a: Array<Array<Int>>, _ b: Array<Array<Int>>) -> Array<Array<Int>> {
    var result = createEmpty(b[0].count, a.count)

    let aRows = a.count
    let bCols = b[0].count
    let aCols = a[0].count

    for i in 0..<aRows {
        for j in 0..<bCols {
            var value = 0
            for k in 0..<aCols {
                value += a[i][k] * b[k][j]
            }
            result[i][j] = value
        }
    }

    return result
}
