import Darwin

let intSize = 8

func greeting() {
    let text = """
    Программа для умножения матриц стандартным алгоритмом и его параллельной версией.
    """
    print(text)
}

func showMenu() {
    let text = """
    Набор команд:
    1 - ввести матрицу A
    2 - ввести матрицу B
    3 - сгенерировать случайную матрицу
    4 - стандартный алгоритм с распараллеливанием
    5 - стандартный алгоритм
    6 - показать меню
    7 - выйти
    """
    print(text)
}

func dataIsValid(_ a: Array<Array<Int>>, _ b: Array<Array<Int>>) -> Bool {
    guard a[0].count == b.count else {
        print("Размерность матриц не допускает умножения!")
        return false
    }
    
    return true
}

func showMatrix(_ matrix: Array<Array<Int>>) {
    for i in 0..<matrix.count {
        for j in 0..<matrix[i].count {
            print(matrix[i][j], terminator: " ")
        }

        print()
    }
}

func inputMatrix(_ matrix:inout Array<Array<Int>>) {
    matrix = []
    print("Введите кол-во строк")
    let a = Int(readLine()!)!
    print("Введите кол-во столбцов")
    let b = Int(readLine()!)!

    for _ in 0..<a {
        var row = [Int]()
        
        _ = readLine()?.split(separator: " ").map {
            if let x = Int($0) {
                row.append(x)
            }
            else {
                print("Неверный ввод")
            }
        }

        if (row.count != b) {
            print("Введено неверное кол-во элементов в строке")
        }

        matrix.append(row)
    }
}

func generateRandom(rows: Int, cols: Int) -> [[Int]] {
    var matrix: [[Int]] = []

    for i in 0..<rows {
        matrix.append([])
        for _ in 0..<cols {
            matrix[i].append(Int.random(in: -5...5))
        }
    }

    return matrix
}

var a: Array<Array<Int>> = []
var b: Array<Array<Int>> = []
var loop = true

greeting()
showMenu()

while loop {
    let input = readLine()!
    let commandNumber = Int(input)
   
    let command = Commands(rawValue: commandNumber! - 1)

    switch command {
    case .inputMatrixA:
        inputMatrix(&a)
    case .inputMatrixB:
        inputMatrix(&b)
    case .generateRandomMatrices:
        print("Введите кол-во строк")
        let rows = Int(readLine()!)!

        print("Введите кол-во столбцов")
        let cols = Int(readLine()!)!

        a = generateRandom(rows: rows, cols: cols)
        b = generateRandom(rows: rows, cols: cols)
    case .standartDispatchedAlgorithm:
        if !dataIsValid(a, b) {
            loop = false
            break
        }

        print("Введите количество потоков:")
        let threadNumber = Int(readLine()!)!

        print("Параллельный алгоритм")

        if #available(macOS 10.12, *) {
            let start = mach_absolute_time()
            let result = standartAlgorithm(a, b, threadNumber: threadNumber)
            let end = mach_absolute_time()

            print("Ожидаемый результат:")
            showMatrix(result)

            print("Результат:")
            showMatrix(result)
            print("Процессорное время(тики):", end - start)
        }
    case .standartAlgorithm:
        if !dataIsValid(a, b) {
            loop = false
            break
        }

        print("Алгоритм без распараллеливания")

        let start = mach_absolute_time()
        let result = standartAlgorithm(a, b)
        let end = mach_absolute_time()

        print("Ожидаемый результат:")
        showMatrix(result)

        print("Результат:")
        showMatrix(result)
        print("Время в тиках:", end - start)
    case .showMenu:
        showMenu()
    case .exit:
        loop = false
    default:
        ()
    }
}
