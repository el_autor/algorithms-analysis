enum Commands: Int {
    case inputMatrixA
    case inputMatrixB
    case generateRandomMatrices
    case standartDispatchedAlgorithm
    case standartAlgorithm
    case showMenu
    case exit
}
