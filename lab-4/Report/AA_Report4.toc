\babel@toc {russian}{}
\contentsline {chapter}{Введение}{2}%
\contentsline {paragraph}{ }{2}%
\contentsline {chapter}{\numberline {1}Аналитическая часть}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {section}{\numberline {1.1}Параллельное программирование}{3}%
\contentsline {paragraph}{ }{3}%
\contentsline {paragraph}{ }{4}%
\contentsline {subsection}{\numberline {1.1.1}Организация взаимодействия параллельных потоков}{4}%
\contentsline {paragraph}{ }{4}%
\contentsline {section}{\numberline {1.2}Вывод}{4}%
\contentsline {paragraph}{ }{4}%
\contentsline {chapter}{\numberline {2}Конструкторская часть}{5}%
\contentsline {paragraph}{ }{5}%
\contentsline {section}{\numberline {2.1}Схемы алгоритмов}{5}%
\contentsline {paragraph}{ }{5}%
\contentsline {section}{\numberline {2.2}Распараллеливание программы}{7}%
\contentsline {paragraph}{ }{7}%
\contentsline {section}{\numberline {2.3}Вывод}{7}%
\contentsline {paragraph}{ }{7}%
\contentsline {chapter}{\numberline {3}Технологическая часть}{8}%
\contentsline {paragraph}{ }{8}%
\contentsline {section}{\numberline {3.1}Выбор ЯП}{8}%
\contentsline {paragraph}{ }{8}%
\contentsline {section}{\numberline {3.2}Листинг кода алгоритмов}{8}%
\contentsline {paragraph}{ }{8}%
\contentsline {section}{\numberline {3.3}Тестирование разработанных функций}{11}%
\contentsline {paragraph}{ }{11}%
\contentsline {paragraph}{}{13}%
\contentsline {section}{\numberline {3.4}Вывод}{14}%
\contentsline {chapter}{\numberline {4}Исследовательская часть}{15}%
\contentsline {paragraph}{ }{15}%
\contentsline {section}{\numberline {4.1}Сравнение временных затрат алгоритмов}{15}%
\contentsline {paragraph}{ }{15}%
\contentsline {section}{\numberline {4.2}Вывод}{16}%
\contentsline {paragraph}{ }{16}%
\contentsline {chapter}{Заключение}{17}%
\contentsline {paragraph}{ }{17}%
\contentsline {chapter}{Список литературы}{17}%
