import XCTest

import lab_4Tests

var tests = [XCTestCaseEntry]()
tests += lab_4Tests.allTests()
XCTMain(tests)
