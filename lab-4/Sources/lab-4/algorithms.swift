import Foundation

func createEmpty(_ rows: Int, _ cols: Int) -> Array<Array<Int>>{
    var matrix: Array<Array<Int>> = []

    for _ in 0..<rows {
        matrix.append(Array(repeating: 0, count: cols))
    }

    return matrix
}

@available(macOS 10.12, *)
class AlgorithmContainer {
    func standartAlgorithm(_ a: Array<Array<Int>>, _ b: Array<Array<Int>>, threadNumber: Int) -> Array<Array<Int>> {
        var result = createEmpty(b[0].count, a.count)

        let aRows = a.count
        let bCols = b[0].count
        let aCols = a[0].count

        var threads: [Thread] = []
        for i in 0..<threadNumber {
            threads.append(Thread{})
            threads[i].start()
        }
        
        for i in 0..<aRows {
            for j in 0..<bCols {
                var emptyThreadFound = false

                while (!emptyThreadFound) {
                    for tNum in 0..<threadNumber {
                        if (threads[tNum].isFinished) {
                            threads[tNum] = Thread {
                                var value = 0
                                
                                for k in 0..<aCols {
                                    value += a[i][k] * b[k][j]
                                }

                                result[i][j] = value
                            }
                            threads[tNum].start()
                            emptyThreadFound = true
                            break
                        }
                    }
                }
            }
        }

        var tasksFinished = false

        while (!tasksFinished) {
            var flag = false

            for tNum in 0..<threadNumber {
                if (!threads[tNum].isFinished) {
                    flag = true
                    break
                }
            }

            if !flag {
                tasksFinished = true
            }
        }

        return result
    }
}

func standartAlgorithm(_ a: Array<Array<Int>>, _ b: Array<Array<Int>>) -> Array<Array<Int>> {
    var result = createEmpty(b[0].count, a.count)

    let aRows = a.count
    let bCols = b[0].count
    let aCols = a[0].count

    for i in 0..<aRows {
        for j in 0..<bCols {
            var value = 0
            for k in 0..<aCols {
                value += a[i][k] * b[k][j]
            }
            result[i][j] = value
        }
    }

    return result
}